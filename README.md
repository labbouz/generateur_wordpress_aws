# Générateur WordPress AWS

## Description du Projet
Ce projet facilite le déploiement automatisé d'un site WordPress sur l'infrastructure Amazon Web Services (AWS) en utilisant Terraform pour provisionner les ressources nécessaires et Docker pour orchestrer les conteneurs Nginx et WordPress.

## Architecture
Le projet est structuré comme suit:
- **Terraform** : Scripts pour provisionner des instances EC2, des groupes de sécurité, des adresses IP élastiques (EIP) et d'autres ressources nécessaires.
- **Docker** : Conteneurs pour exécuter Nginx et WordPress, avec une configuration prête pour le support SSL via Let's Encrypt.

### Répertoire du Projet
```
project-root/
|-- terraform/
|   |-- variables.tf
|   |-- outputs.tf
|   |-- provider.tf
|   |-- security_groups.tf
|   |-- ec2_instance.tf
|   |-- eip.tf
|   |-- terraform.tfvars
|-- docker/
|   |-- nginx/
|   |   |-- Dockerfile
|   |   |-- default.conf
|   |   |-- entrypoint.sh
|   |-- docker-compose.yml
|   |-- nginx-proxy.conf
|   |-- custom.ini
|-- setup.sh
|-- README.md
```

## Prérequis
- Compte AWS avec les droits nécessaires.
- [Terraform](https://www.terraform.io/downloads.html) installé localement.
- [Docker](https://www.docker.com/products/docker-desktop) et [Docker Compose](https://docs.docker.com/compose/install/).
- Configuration du CLI AWS avec vos identifiants.

## Configuration initiale
### Configuration de l'AWS CLI
Configurez votre AWS CLI avec vos identifiants AWS :
```bash
aws configure
```

### Clonage du projet
Clonez le dépôt et naviguez dans le répertoire du projet :
```bash
git clone https://gitlab.com/labbouz/generateur_wordpress_aws.git
cd generateur_wordpress_aws
```

### Configuration de Terraform
Initialisez Terraform pour télécharger les dépendances nécessaires :
```bash
terraform init
```

## Déploiement avec Terraform
Appliquez la configuration Terraform pour déployer l'infrastructure :
```bash
terraform apply
```

## Configuration DNS chez OVH
Pour associer le sous-domaine `demo.site.dev` à votre instance AWS :
1. Connectez-vous à votre espace client OVH.
2. Naviguez vers la section "Domaines" et sélectionnez `site.dev`.
3. Ajoutez un enregistrement A pointant vers l'adresse IP élastique provisionnée par Terraform.

## Gestion des Certificats SSL
Les certificats SSL sont gérés automatiquement via Let's Encrypt, configuré dans les fichiers Docker et Nginx pour renouveler les certificats et les appliquer sans interruption de service.

## Accès et Utilisation
Une fois déployé, accédez à votre site WordPress via `https://demo.site.dev` pour commencer la configuration de votre site.

## Contribution
Les contributions sont les bienvenues. Veuillez soumettre vos pull requests ou vos issues via GitLab.

## Licence
Ce projet est publié sous la licence MIT, permettant une large utilisation, modification et distribution.
