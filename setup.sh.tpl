#!/bin/bash
sudo yum update -y
sudo amazon-linux-extras install docker
sudo systemctl start docker
sudo systemctl enable docker
sudo usermod -aG docker $USER

curl -L "https://github.com/docker/compose/releases/download/v2.5.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

# Installer Git
sudo yum install git -y

# Cloner le dépôt contenant les configurations Docker
git clone https://gitlab.com/labbouz/generateur_wordpress_aws.git /home/ec2-user/app-wp

echo "email=${email}" > /home/ec2-user/app-wp/docker/.env 
echo "domain=${domain}" >> /home/ec2-user/app-wp/docker/.env
echo "MYSQL_ROOT_PASSWORD=${mysql_root_password}" >> /home/ec2-user/app-wp/docker/.env 
echo "MYSQL_PASSWORD=${mysql_password}" >> /home/ec2-user/app-wp/docker/.env


# Naviguer dans le répertoire cloné
cd /home/ec2-user/app-wp/docker

sudo /usr/local/bin/docker-compose up -d
