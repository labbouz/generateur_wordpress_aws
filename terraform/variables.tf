variable "aws_region" {
  description = "AWS region to deploy resources"
  default     = "eu-west-1"
}

variable "instance_type" {
  description = "EC2 instance type"
  default     = "t3.micro"
}

variable "domain" {
  description = "Domain name for the WordPress site"
  type        = string
}

variable "email" {
  description = "Email address for SSL certificate registration"
  type        = string
}

variable "ssh_public_key_path" {
  description = "Path to the SSH public key to be uploaded to AWS"
  type        = string
}

variable "ami" {
  description = "Utilisez l'AMI appropriée"
  type        = string
}

variable "mysql_root_password" {
  description = "Password for the MySQL root user"
  type        = string
  sensitive   = true
}

variable "mysql_password" {
  description = "Password for the WordPress MySQL user"
  type        = string
  sensitive   = true
}
