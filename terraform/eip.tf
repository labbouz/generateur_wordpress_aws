resource "aws_eip" "web" {
  # Aucun attribut vpc nécessaire
  tags = {
    Name = "WP EIP"
  }

  lifecycle {
    prevent_destroy = true
  }
}