# resource "aws_key_pair" "deploy" {
#   key_name = "wordpressDemoKey"
#   public_key = file(var.ssh_public_key_path)
# }


resource "aws_instance" "web" {
  ami           = var.ami
  instance_type = var.instance_type
  # key_name      = aws_key_pair.deploy.key_name
  key_name = "wordpressDemoKey"

  user_data = templatefile("${path.module}/../setup.sh.tpl", {
    mysql_root_password = var.mysql_root_password
    mysql_password      = var.mysql_password
    email               = var.email
    domain              = var.domain
  })


  vpc_security_group_ids = [aws_security_group.web.id]

  tags = {
    Name = "WordPressInstance"
  }

  metadata_options {
    http_tokens = "required" # Active IMDSv2 en exigeant l'utilisation de jetons HTTP
  }
}

resource "aws_eip_association" "eip_assoc" {
  instance_id   = aws_instance.web.id
  allocation_id = aws_eip.web.id
}




