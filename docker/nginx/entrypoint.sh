#!/bin/bash

# Chemin où les certificats SSL doivent être stockés
CERT_PATH="/etc/letsencrypt/live/demo.labbouz.dev/fullchain.pem"
KEY_PATH="/etc/letsencrypt/live/demo.labbouz.dev/privkey.pem"

# Attente passive pour que les certificats soient disponibles avant de démarrer Nginx
echo "En attente des certificats SSL à $CERT_PATH..."
while [ ! -f "$CERT_PATH" ] || [ ! -f "$KEY_PATH" ]
do
    sleep 1
done
echo "Certificats SSL trouvés. Démarrage de Nginx..."

# Démarrer Nginx en mode foreground
nginx -g 'daemon off;'
